# Finding angle of arrival (Azimuth Angle)

import numpy as np
import h5py
import random
import matplotlib.pyplot as plt
import tensorflow as tf
#from tensorflow.keras.models import load_model
from tensorflow import keras
#from tensorflow.keras.models import Sequential


 

filename = h5py.File("data.h5", 'r')
filename.keys()
with h5py.File(filename, 'r') as f:
    a_group_key = list(f.keys())[0]
    data = list(f[a_group_key])

"""
#J Wintrode 
allX = filename['X'] # list of 200-sample vectors
allY = filename['Y'] # list of 0,1 'labels'

trainX = []; testX = []
trainY = []; testY = []

N = len(allX)
indexes = list(range(N))
np.random.shuffle(indexes)

M = int(N* 0.2) # 80/20 train/test split

for idx in indexes[0:M] :
    testX.append(allX[idx])
    testY.append(allY[idx])


for idx in indexes[M:] :
    trainX.append(allX[idx])
    trainY.append(allY[idx])


# ok, now we're ready to train.
trainX = np.array(trainX)
trainY = np.array(trainY)
testX = np.array(testX)
testY = np.array(testY)

model.fit(trainX, trainY, epochs=40, batch_size=64,
          validation_data=(testX, testY))

# here we go.

results = model.evaluate(testX, testY, batch_size=128)
print("test loss, test acc:", results)
"""


#----------------------------------------------------------------
def ant_geometry():
    layers = [keras.layers.Flatten(input_shape=(28, 28)), 
          keras.layers.Dense(128, activation='relu'), 
          keras.layers.Dense(10, activation='softmax')]
    model = keras.Sequential(layers[0])
    #model.add(layers[0])
    model.compile(optimizer='adam', 
              loss='sparse_categorical_crossentropy', 
              metrics=['accuracy'])
    from tensorflow.keras.models import load_model
    model.save("data.h5")
    print("saved model to disk")
    #Load model
    #model = load_model('data.h5')
    array_geometry = load_model('data.h5')
    #model1 = load_model("data.h5")
    #with h5py.File(r'data.h5') as file:
    #    array_geometry = h5py.load(file, h5py.FullLoader)
    
    ant_elements = []
    for i in array_geometry.values():    
        ant_elements.extend(i)
    
    # Create ant_elements which contains x,y,z coordinates for each array element
    ant_elements = [float(i) for i in ant_elements]

    return ant_elements
#----------------------------------------------------------------    
ant_elements = ant_geometry()

def f_time_ave_n(mean, sample_size, MP, antenna_dist):


    ant_elements = []
    for i in array_geometry.values():    
        ant_elements.extend(i)
    
    ant_elements = [float(i) for i in ant_elements]

    # Create Receiver element array (x,y-location)
    rxy = zeros(3)
    rxy[1] = ant_elements[1] # Middle rx element y-location
    rxy[0] = ant_elements[4] # location of top rx element
    rxy[2] = ant_elements[7] # location of bottom rx element
    rxx = zeros(3)
    rxx[1] = ant_elements[0]
    rxx[0] = ant_elements[3]
    rxx[2] = ant_elements[6]


    # Azimuth angle
    az_ang = None
    # Transmitting frequency
    ft = 3.55e9 # 3.55 GHz
    # frequency array
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)

    # Sampling frequency
    fs = 5e6 # 5 MHz

    # Speed of light
    c = 299792458 # m/s

    # Wavelength (lambda)
    w = c/ft

    # Number of samples
    n_samp = 1024

    # Noise Power
    noise_power = 0.01

    # Direction of signals
    ang1 = [90, 73]
    ang2 = [90, 68]
    angs = [ang1, ang2]

    # Create Distance array
    if mp == 1:
        D = np.zeros(6)
    else:
        D = np.zeros(3)

    # Calculate Distances and time delays of Multipath and lOS
    if MP == 1:
        D[0] = np.sqrt( (l+rxx)**2 + (rxy[0]+m_bounce+(m_bounce+tx))**2 )
        D[1] = np.sqrt( (l+rxx)**2 + (rxy[1]+m_bounce+(m_bounce+tx))**2 )
        D[2] = np.sqrt( (l+rxx)**2 + (rxy[2]+m_bounce+(m_bounce+tx))**2 )
        D[3] = np.sqrt((l+rxx)**2 + (tx-rxy[0])**2)
        D[4] = np.sqrt((l+rxx)**2 + (tx-rxy[1])**2)
        D[5] = np.sqrt((l+rxx)**2 + (tx-rxy[2])**2)
    else:
        D[0] = np.sqrt((l+rxx)**2 + (tx-rxy[0])**2)
        D[1] = np.sqrt((l+rxx)**2 + (tx-rxy[1])**2)
        D[2] = np.sqrt((l+rxx)**2 + (tx-rxy[2])**2)


    # Time delay
    td = D/c
    # Minimum time delay
    min_td = np.min(td)
    #Total time delay
    total_td = td - min_td

    













